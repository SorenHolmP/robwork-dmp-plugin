#ifndef SAMPLEPLUGIN_HPP
#define SAMPLEPLUGIN_HPP


//c++ stdlib:
#include <iostream>
#include <vector>
#include <fstream>

//RobWork
#include <RobWorkStudioConfig.hpp> // For RWS_USE_QT5 definition
#include <rws/RobWorkStudioPlugin.hpp>
#include <rw/rw.hpp>
#include <rwlibs/opengl/RenderGeometry.hpp>
#include <rwlibs/opengl/DrawableFactory.hpp>


#include "ui_SamplePlugin.h"

//Qt
#include <QTimer>
#include <QDebug>

//shp
#include "mypathplanner.hpp"

using namespace rw::loaders;




class SamplePlugin: public rws::RobWorkStudioPlugin, private Ui::SamplePlugin
{
Q_OBJECT
Q_INTERFACES( rws::RobWorkStudioPlugin )
#if RWS_USE_QT5
Q_PLUGIN_METADATA(IID "dk.sdu.mip.Robwork.RobWorkStudioPlugin/0.1" FILE "plugin.json")
#endif
public:
    SamplePlugin();
	virtual ~SamplePlugin();

    virtual void open(rw::models::WorkCell* workcell);

    virtual void close();

    virtual void initialize();

    std::vector<rw::math::Transform3D<>> getPosesFromFile(std::string path);
    std::vector<rw::math::Q> pathPlan(rw::math::Q q_start, rw::math::Q q_end);

private slots:
    void on_rewindButton_clicked();
    void on_startSolutionSpinBox_valueChanged(int arg1);
    void on_playPauseTrajectoryButton_clicked();

    void on_timerSlider_sliderMoved(int position);

    void on_inputTrajectoryButton_clicked();
    void stateChangedListener(const rw::kinematics::State& state);
    void timerFunc();

private:

    rws::RobWorkStudio*                     _rws;
    rw::models::WorkCell::Ptr               _wc;
    rw::models::SerialDevice::Ptr           _device;
    rw::kinematics::State                   _state;
    rw::invkin::ClosedFormIKSolverUR*       _IKSolverUR;
    rw::proximity::CollisionDetector::Ptr   _collisionDetector;
    QTimer* _timer;


    //Visualization:
    std::vector<rw::math::Transform3D<>> _trajectory_transforms;
    std::vector<rw::math::Q>    _qPath;
    int                         _qPathIdx = 0;
    bool                        _bool_playTrajectory = false;

    //Pathplanning:
    MyPathPlanner* _pp;


};

#endif /*RINGONHOOKPLUGIN_HPP_*/
