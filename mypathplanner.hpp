#ifndef MYPATHPLANNER_H
#define MYPATHPLANNER_H

#include <rw/rw.hpp>
#include <rwlibs/pathplanners/rrt/RRTQToQPlanner.hpp>
#include <rwlibs/pathplanners/rrt/RRTPlanner.hpp>
#include <rwlibs/proximitystrategies/ProximityStrategyFactory.hpp>



#include <rwlibs/proximitystrategies/ProximityStrategyYaobi.hpp>
#include <rwlibs/pathplanners/sbl/SBLPlanner.hpp>

class MyPathPlanner
{
public:
    MyPathPlanner();
    MyPathPlanner(rw::kinematics::State                   state,
                rw::models::WorkCell::Ptr               wc,
                rw::models::SerialDevice::Ptr           device,
                rw::invkin::ClosedFormIKSolverUR*       IKS,
                rw::proximity::CollisionDetector::Ptr   cd);

//    std::vector<rw::math::Q> findPath(std::vector<rw::math::Transform3D<>> desired_transforms, int start_configuration );
    std::vector<rw::math::Q> invKin_OnListOfTransforms(std::vector<rw::math::Transform3D<> > transforms, int start_configuration);



    std::vector<rw::math::Q> pathPlanRRT(rw::math::Q q_start, rw::math::Q q_end);
    std::vector<rw::math::Q> pathPlanSBL(rw::math::Q q_start, rw::math::Q q_end);




private:
    rw::kinematics::State                       _state;
    rw::models::WorkCell::Ptr                   _wc;
    rw::models::SerialDevice::Ptr               _device;
    rw::invkin::ClosedFormIKSolverUR*           _IKSolverUR;
    rw::proximity::CollisionDetector::Ptr       _collision_detector;
};

#endif // MYPATHPLANNER_H
