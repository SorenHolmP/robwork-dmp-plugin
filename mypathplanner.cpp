#include "mypathplanner.hpp"

#include <QDebug>


using namespace rw::models;
using namespace rw::invkin;
using namespace rw::proximity;
using namespace rw::kinematics;
using namespace rw::math;

using namespace rw::pathplanning; //PlannerConstraint
using namespace rwlibs::pathplanners;
using namespace rw::trajectory; //QPath

using namespace Eigen;
using namespace std;

const int MAX_TIME = 10;
using namespace rw::loaders;
using namespace rw::models;
using namespace rw::pathplanning; //PlannerConstraint
using namespace rw::proximity;
using namespace rw::trajectory; //QPath
using namespace rwlibs::pathplanners;
using namespace rwlibs::proximitystrategies;

using rwlibs::proximitystrategies::ProximityStrategyYaobi;
using rw::proximity::CollisionDetector;



MyPathPlanner::MyPathPlanner()
{

}

MyPathPlanner::MyPathPlanner(State state, WorkCell::Ptr wc, SerialDevice::Ptr device, ClosedFormIKSolverUR *IKS, CollisionDetector::Ptr cd):
    _state(state), _wc(wc), _device(device), _IKSolverUR(IKS), _collision_detector(cd)
{
}

std::vector<Q> MyPathPlanner::invKin_OnListOfTransforms(std::vector<rw::math::Transform3D<> > transforms, int start_configuration)
{
    std::vector<Q> qPath;
    for(auto T : transforms)
    {
        std::vector<Q> solution = _IKSolverUR->solve(T, _state);
        if(solution.size() == 0)
        {
            cout << "could not find inverse kinematics solution for\n";
            cout << T << endl;
            RW_THROW("Missing inverse kinematics solution");
        }
        qPath.push_back(solution[start_configuration]);
    }

    return qPath;
}


std::vector<Q> MyPathPlanner::pathPlanRRT(Q q_start, Q q_goal)
{
    _device->setQ(q_start,_state);

    CollisionDetector detector(_wc, ProximityStrategyFactory::makeDefaultCollisionStrategy());
    PlannerConstraint constraint = PlannerConstraint::make(&detector,_device,_state);

    QToQPlanner::Ptr planner = RRTPlanner::makeQToQPlanner(constraint, _device, RRTPlanner::RRTConnect);
    QPath path;
    bool ok = planner->query(q_start, q_goal, path);

    if(!ok)
    {
        cout << "Could not find a path: " << endl;
    }
    else
    {
        cout << "Found a path RRT: " << endl;
        cout << path.size() << endl;
        for(int i = 0; i < path.size(); i++)
            cout << path[i] << endl;
        return path;
    }
}


std::vector<Q> MyPathPlanner::pathPlanSBL(Q q_start, Q q_goal)
{

    _device->setQ(q_start,_state);

    // The q constraint is to avoid collisions.
    CollisionDetector coldetect(_wc, ProximityStrategyYaobi::make());
    QConstraint::Ptr constraint = QConstraint::make(&coldetect, _device, _state);

    // the edge constraint tests the constraint on edges, eg. edge between two configurations
    QEdgeConstraintIncremental::Ptr edgeconstraint = QEdgeConstraintIncremental::makeDefault(
        constraint, _device);

    // An SBL based point-to-point path planner.
    QToQPlanner::Ptr planner = SBLPlanner::makeQToQPlanner(
        SBLSetup::make(constraint, edgeconstraint, _device));

    QPath path;
    bool ok = planner->query(q_start, q_goal, path);

    if(!ok)
    {
        cout << "Could not find a path: " << endl;
    }
    else
    {
        cout << "Found a path SBL: " << endl;
        cout << path.size() << endl;
        return path;
    }



}

