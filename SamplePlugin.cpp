#include "SamplePlugin.hpp"

#include <RobWorkStudio.hpp>
#include <rw/loaders/path/PathLoader.hpp>

#include <boost/bind.hpp>
#include <string>

#include <stdlib.h>

//Namespaces
using namespace std;

using rw::kinematics::State;
using rw::kinematics::Frame;
using rw::kinematics::MovableFrame;
using rw::models::WorkCell;
using rws::RobWorkStudioPlugin;
using namespace rw::loaders;
using namespace rw::common;
using namespace rw::math;
using rw::geometry::Geometry;

using namespace rwlibs::opengl;

#if !RWS_USE_QT5
#include <QtCore/qplugin.h>
Q_EXPORT_PLUGIN(SamplePlugin);
#endif

SamplePlugin::SamplePlugin():
    RobWorkStudioPlugin("SamplePluginUI", QIcon(":/pa_icon.png"))
{
    setupUi(this);
    timerSlider->setMaximum(1000);
    timerSlider->setMinimum(1);
    label->text();
}

SamplePlugin::~SamplePlugin()
{
    delete _pp;
}

void SamplePlugin::initialize() {

    getRobWorkStudio()->stateChangedEvent().add(boost::bind(&SamplePlugin::stateChangedListener, this, _1), this);

    cout << "Hello from DMP_simulator initialize()" << endl;
    _rws = getRobWorkStudio();
    _rws->openFile("/home/soren/hobby/rw_place_box_ui/workcell/workcell.xml");
    _wc = _rws->getWorkCell();

    //_device = _wc->findDevice<rw::models::SerialDevice>("UR-6-85-5-A");
    _device = _wc->findDevice<rw::models::SerialDevice>("UR5_2017");
    _state = _wc->getDefaultState();
    _IKSolverUR = new rw::invkin::ClosedFormIKSolverUR(_device, _state);
    _collisionDetector = _rws->getCollisionDetector();

    if(_wc.isNull())
        RW_THROW("WorkCell not loaded properly");
    if(_device.isNull())
        RW_THROW("Device not found");

    _timer = new QTimer(this);
    connect(_timer,SIGNAL(timeout()), this, SLOT(timerFunc()));
    _timer->start(20);

    _pp = new MyPathPlanner(_state, _wc, _device, _IKSolverUR, _collisionDetector);
}

void SamplePlugin::open(WorkCell* workcell)
{
    cout << "HELLO WORLD" << endl;
    _wc = workcell;
    //_device = _wc->findDevice<rw::models::SerialDevice>("UR5_2017");
//    _device = _wc->getDevices()[0];
//    cout << "wc->getDevices()[0]: " << _wc->getDevices()[0]->getName() << endl;
    _state = _wc->getDefaultState();
    _IKSolverUR = new rw::invkin::ClosedFormIKSolverUR(_device, _state);
//    _collisionDetector = _rws->getCollisionDetector();

//    delete _pp;
//    _pp = new MyPathPlanner(_state, _wc, _device, _IKSolverUR, _collisionDetector);


}

void SamplePlugin::close() {
}



void SamplePlugin::stateChangedListener(const State& state)
{
    _state = state;
}

void SamplePlugin::timerFunc()
{
    if(_bool_playTrajectory && _qPathIdx < _qPath.size() )
    {
        _device->setQ(_qPath[_qPathIdx++], _state);
        _rws->setState(_state);
    }
}



vector<rw::math::Transform3D<>> SamplePlugin::getPosesFromFile(string path)
{
    //std::setlocale(LC_ALL, "C"); //https://www.reddit.com/r/cpp/comments/2e68nd/stdstod_is_locale_dependant_but_the_docs_does_not/

    vector<rw::math::Transform3D<>> transforms;
    vector<double> poses;

    ifstream file(path);
    std::string word;
    if(file.is_open())
    {
        while(file >> word)
        {
           poses.push_back(stod(word));
        }
    }
    else
        std::cout << "Could not open poses file" << std::endl;

    for(uint i = 0; i < poses.size() / 6; i++) //There are 6 entries per line, format XYZ RPY
    {
        int idx = i*6;
        double x = poses[idx], y = poses[idx+1], z = poses[idx+2], R = poses[idx+3], P = poses[idx+4], Y = poses[idx+5];

        rw::math::Vector3D<> V(x, y, z); //displacement
        rw::math::RPY<> rpy(R,P,Y);     //Rotation matrix
        rw::math::Transform3D<double> T(V, rpy.toRotation3D());
        transforms.push_back(T);
    }

    return transforms;
}


void SamplePlugin::on_inputTrajectoryButton_clicked()
{
    _bool_playTrajectory = false;
    _qPathIdx = 0;

    system("cd /home/soren/9sem/speciale/matlab/DMP_OOP && matlab -nodesktop -nosplash -r \"dmp2; exit;\" | tail -n 11");

    _trajectory_transforms = getPosesFromFile("/home/soren/hobby/rw_place_box_ui/data/trajectories/y_track_transforms.txt");

    int start_config = Ui_SamplePlugin::startSolutionSpinBox->value();
    _qPath = _pp->invKin_OnListOfTransforms(_trajectory_transforms, start_config);
}



void SamplePlugin::on_timerSlider_sliderMoved(int position)
{
    cout << "timer slider position: " << position << endl;
    _timer->setInterval(position);
}

void SamplePlugin::on_playPauseTrajectoryButton_clicked()
{
    _bool_playTrajectory = !_bool_playTrajectory;
}


void SamplePlugin::on_startSolutionSpinBox_valueChanged(int arg1)
{
    _qPath = _pp->invKin_OnListOfTransforms(_trajectory_transforms, arg1);
    if(_qPathIdx < _qPath.size())
    {
        _device->setQ(_qPath[_qPathIdx], _state);
        _rws->setState(_state);
    }
    else
    {
        _device->setQ(_qPath[_qPathIdx - 1], _state);
        _rws->setState(_state);
    }
}

void SamplePlugin::on_rewindButton_clicked()
{
    _qPathIdx = 0;

}
